﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Codility.Respectable
{
    public class SantasSleigh
    {
        /*
You're airdropped near Easter Bunny Headquarters in a city somewhere. "Near", unfortunately, is as close as you can get - the instructions on the Easter Bunny Recruiting Document the Elves intercepted start here, and nobody had time to work them out further.

The Document indicates that you should start at the given coordinates (where you just landed) and face North. Then, follow the provided sequence: either turn left (L) or right (R) 90 degrees, then walk forward the given number of blocks, ending at a new intersection.

There's no time to follow such ridiculous instructions on foot, though, so you take a moment and work out the destination. Given that you can only walk on the street grid of the city, how far is the shortest path to the destination?

For example:

Following R2, L3 leaves you 2 blocks East and 3 blocks North, or 5 blocks away.
R2, R2, R2 leaves you 2 blocks due South of your starting position, which is 2 blocks away.
R5, L5, R5, R3 leaves you 12 blocks away.
How many blocks away is Easter Bunny HQ?

--- Part Two ---

Then, you notice the instructions continue on the back of the Recruiting Document. Easter Bunny HQ is actually at the first location you visit twice.

For example, if your instructions are R8, R4, R4, R8, the first location you visit twice is 4 blocks away, due East.

How many blocks away is the first location you visit twice?
         */

        public int CalculateDistance(List<Coord> coords)
        {
            XyCoord currentCoord = new XyCoord();
            EarthDirection? lastDirection = null;
            foreach (Coord coord in coords)
            {
                EarthDirection newDirection;
                if (lastDirection == null)
                {
                    newDirection = EarthDirection.N;
                }
                else
                {
                    newDirection = CalculateNewDirection(lastDirection.Value, coord.Direction);
                }

                switch (newDirection)
                {
                    case EarthDirection.N:
                        currentCoord.Y += coord.Distance;
                        break;
                    case EarthDirection.E:
                        currentCoord.X += coord.Distance;
                        break;
                    case EarthDirection.S:
                        currentCoord.Y -= coord.Distance;
                        break;
                    case EarthDirection.W:
                        currentCoord.X -= coord.Distance;
                        break;
                }

                lastDirection = newDirection; 
            }
            return Math.Abs(currentCoord.X) + Math.Abs(currentCoord.Y);
        }


        public int? CalculateDistancePhase2(List<Coord> coords)
        {

			XyCoord currentCoord = new XyCoord();
            List<XyCoord> pastLocationsVisited = new List<XyCoord>();
			EarthDirection? lastDirection = null;
			foreach (Coord coord in coords)
			{

                EarthDirection newDirection;
				if (lastDirection == null)
				{
					newDirection = EarthDirection.N;
				}
				else
				{
					newDirection = CalculateNewDirection(lastDirection.Value, coord.Direction);
				}


				for (int i = 0; i < coord.Distance; i++)
                {
                    switch (newDirection)
                    {
                        case EarthDirection.N:
                            currentCoord.Y += 1;
                            break;
                        case EarthDirection.E:
                            currentCoord.X += 1;
                            break;
                        case EarthDirection.S:
                            currentCoord.Y -= 1;
                            break;
                        case EarthDirection.W:
                            currentCoord.X -= 1;
                            break;
                    }

                    lastDirection = newDirection;

                    if (pastLocationsVisited.Any(p => p.X == currentCoord.X && p.Y == currentCoord.Y))
                    {
    					//we've been here before
    					return Math.Abs(currentCoord.X) + Math.Abs(currentCoord.Y);
                    }
    					pastLocationsVisited.Add(new XyCoord() { X = currentCoord.X, Y = currentCoord.Y });
    				}

			}
            //We went through the whole route and didn't re-visit.
            throw new Exception("Never visited the same spot twice.");
        }

        public static List<Coord> CreateCords(string coordData)
        {
            List<Coord> coords = new List<Coord>();
            List<string> strings = coordData.Split(',').Select(s => s.Trim()).ToList();
            foreach (string coor in strings)
            {
                int distance;
                if (!int.TryParse(coor.Substring(1, coor.Length - 1), out distance))
                {
                    throw new ArgumentException("Cannot parse string.");
                }

                Coord coord = new Coord(distance, (Direction)Enum.Parse(typeof(Direction), coor.Substring(0, 1)));
                coords.Add(coord);
            }
            return coords;
        }

        public class Coord
        {
            public Coord(int distance, Direction direction)
            {
                Distance = distance;
                Direction = direction;
            }

            public override string ToString()
            {
                return string.Format("Coord: Dist={0}, Dir={1}", Distance, Direction);
            }

            public int Distance { get; set; }
            public Direction Direction { get; set; }
        }

        public enum Direction
        {
            L,
            R
        }

        private class XyCoord
        {
            public override string ToString()
            {
                return string.Format("[XyCoord: X={0}, Y={1}]", X, Y);
            }

            public int X { get; set; }
            public int Y { get; set; }
        }

        private enum EarthDirection
        {
            N = 0,
            E = 1,
            S = 2,
            W = 3
        }


		private EarthDirection CalculateNewDirection(EarthDirection currentDirection, Direction nextTurn)
		{
			if (nextTurn == Direction.R)
			{
				return GoRight(currentDirection);
			}
			else if (nextTurn == Direction.L)
			{
				// #bakedgoods
				return GoLeft(currentDirection);
			}
			throw new ArgumentException("Can only go left or right");
		}

		private EarthDirection GoLeft(EarthDirection ed)
		{
			if ((int)ed == 0)
			{
				return EarthDirection.W;
			}

			return (EarthDirection)(ed - 1);
		}

		private EarthDirection GoRight(EarthDirection ed)
		{
			if ((int)ed == 3)
			{
				return EarthDirection.N;
			}

			return (EarthDirection)(ed + 1);
		}
	}


}