﻿using System;
using NUnit.Framework;

namespace UnitTests.Respectable
{
    [TestFixture]
    public class PeaksUnitTests
    {
        [Test]
        public void PeaksTest01()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 1, 2, 2 };
            // peaks: 2
            // { 2, 1, 5, 1}, {2, 1, 2, 2}

            Assert.AreEqual(2, Codility.Respectable.Peaks.PeaksSolution(a, a.Length));
        }

        [Test]
        public void PeaksTest02()
        {
            int[] a = new[] { 1, 2, 1, 1, 2, 1, 2, 2 };
            // { 1, 2, 1, 1}, {2, 1, 2, 2}
            Assert.AreEqual(2, Codility.Respectable.Peaks.PeaksSolution(a, a.Length));
        }

        [Test]
        public void PeaksTest03()
        {
            int[] a = new[] { 1, 2, 1, 1, 2, 1, 1, 2, 1 };
            // { 1, 2, 1}, {1, 2, 1}, {1, 2, 1}
            Assert.AreEqual(3, Codility.Respectable.Peaks.PeaksSolution(a, a.Length));
        }

        [Test]
        public void PeaksTest04()
        {
            int[] a = new[] { 1, 2, 1, 1, 2, 1, 1, 2, 3 };
            Assert.AreEqual(1, Codility.Respectable.Peaks.PeaksSolution(a, a.Length));
        }


        [Test]
        public void PeaksTest05()
        {
            int[] a = new[] { 1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2 };
            //peaks {3, 5, 10}
            // { 1, 2, 3, 4 } , {3, 4, 1, 2} , {3, 4, 6, 2 };
            Assert.AreEqual(3, Codility.Respectable.Peaks.PeaksSolution(a, a.Length));
        }

        [Test]
        public void PeaksTest06()
        {
            int[] a = new[] { 1, 2, 1, 3, 2, 2, 6, 3, 2, 4, 6, 2 };
            //peaks {1, 3, 6, 10}
            // { 1, 2, 1} , {3, 2, 2} , {6, 3, 2} , {4, 6, 2 }
            Assert.AreEqual(4, Codility.Respectable.Peaks.PeaksSolution(a, a.Length));
        }

        [Test]
        public void PeaksTest07()
        {
            int[] a = new[] { 1, 2 };
            //peaks { }
            Assert.AreEqual(0, Codility.Respectable.Peaks.PeaksSolution(a, a.Length));
        }

        [Test]
        public void PeaksTest08()
        {
            int[] a = new[] { 1, 2, 2 };
            //peaks { }
            Assert.AreEqual(0, Codility.Respectable.Peaks.PeaksSolution(a, a.Length));
        }
        [Test]
        public void PeaksTest09()
        {
            int[] a = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 7, 10, 11, 12, 13 };
            Assert.AreEqual(1, Codility.Respectable.Peaks.PeaksSolution(a, a.Length));
        }
    }
}
