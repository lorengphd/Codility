﻿    using System;
using System.Collections.Generic;
using System.Linq;

namespace Codility.Respectable
{
    /*
You are given integers K, M and a non-empty zero-indexed array A consisting of N integers. Every element of the array is not greater than M.

You should divide this array into K blocks of consecutive elements. The size of the block is any integer between 0 and N. Every element of the array should belong to some block.

The sum of the block from X to Y equals A[X] + A[X + 1] + ... + A[Y]. The sum of empty block equals 0.

The large sum is the maximal sum of any block.

For example, you are given integers K = 3, M = 5 and array A such that:
  A[0] = 2
  A[1] = 1
  A[2] = 5
  A[3] = 1
  A[4] = 2
  A[5] = 2
  A[6] = 2

The array can be divided, for example, into the following blocks:

        [2, 1, 5, 1, 2, 2, 2], [], [] with a large sum of 15;
        [2], [1, 5, 1, 2], [2, 2] with a large sum of 9;
        [2, 1, 5], [], [1, 2, 2, 2] with a large sum of 8;
        [2, 1], [5, 1], [2, 2, 2] with a large sum of 6.

The goal is to minimize the large sum. In the above example, 6 is the minimal large sum.

Write a function:

    class Solution { public int solution(int K, int M, int[] A); }

that, given integers K, M and a non-empty zero-indexed array A consisting of N integers, returns the minimal large sum.

For example, given K = 3, M = 5 and array A such that:
  A[0] = 2
  A[1] = 1
  A[2] = 5
  A[3] = 1
  A[4] = 2
  A[5] = 2
  A[6] = 2

the function should return 6, as explained above.

Assume that:

        N and K are integers within the range [1..100,000];
        M is an integer within the range [0..10,000];
        each element of array A is an integer within the range [0..M].

Complexity:

        expected worst-case time complexity is O(N*log(N+M));
        expected worst-case space complexity is O(1), beyond input storage (not counting the storage required for input arguments).

Elements of input arrays can be modified.

    */
    public class MinMaxDisivion
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="k">Represents the amount of blocks that the array (a) should be broken into.</param>
        /// <param name="m">The max size of any element in the arrary (a)</param>
        /// <param name="a"></param>
        /// <returns></returns>
        public static int MinMaxDivision(int k, int m, int[] a)
        {
            List<Block> blocks = CreateBlocks(a, k);
            return blocks.OrderBy(x => x.MaxSum).First().MaxSum;
        }

        /// <summary>
        /// Splits an array into all possible blocks.
        /// </summary>
        /// <param name="inputArray">The original array</param>
        /// <param name="n">The number of blocks to be created</param>
        /// <returns>A list of blocks</returns>
        private static List<Block> CreateBlocks(int[] inputArray, int n)
        {
            if (inputArray.Length == 0)
            {
                return new List<Block> { new Block { TheBlocks = new List<int[]> { new[] { 0 } } } };
            }

            List<Block> blocks = new List<Block>();
            int splitSpot = inputArray.Length;
            for (int i = 0; i < inputArray.Length; i++)
            {
                int[] leftSplit = inputArray.SplitLeft(splitSpot);
                if (leftSplit.Length < inputArray.Length)
                {
                    var splitRight = inputArray.SplitRight(splitSpot);
                    List<Block> subBlocks = CreateBlocks(splitRight, n - 1);
                    foreach (Block subBlock in subBlocks)
                    {
                        Block block = new Block() { TheBlocks = new List<int[]>() };
                        block.TheBlocks.Add(leftSplit);
                        foreach (int[] x in subBlock.TheBlocks)
                        {
                            block.TheBlocks.Add(x);
                        }
                        blocks.Add(block);
                    }
                }
                else
                {
                    blocks.Add(new Block { TheBlocks = new List<int[]> { leftSplit } });
                    if (n == 1)
                    {
                        return blocks;
                    }
                }
                splitSpot--;
            }
            return blocks;
        }

        /// <summary>
        /// A single line of numbers e.g. [1, 2, 3], [4, 5]
        /// </summary>
        private class Block
        {
            public override string ToString()
            {
                return string.Format("Max: {0}. {1}", MaxSum,
                    string.Join(",", string.Format("[ {0} ]",
                    TheBlocks)));
            }

            public List<int[]> TheBlocks { get; set; }

            public int MaxSum
            {
                get
                {
                    return TheBlocks.Max(x => x.Sum());
                }
            }
        }
    }

    public static class Extensions
    {
        public static int[] SplitLeft(this int[] input, int splitSpot)
        {
            int[] output = new int[splitSpot];

            for (int i = 0; i < splitSpot; i++)
            {
                output[i] = input[i];
            }
            return output;
        }
        public static int[] SplitRight(this int[] input, int splitSpot)
        {
            int[] output = new int[Math.Abs(input.Length - splitSpot)];
            //[1, 2, 3 , 4, 5]  3
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = input[i + splitSpot];
            }
            return output;
        }


        /// <summary>
        /// This is the array version of String.SubString()
        /// </summary>
        /// <param name="a"></param>
        /// <param name="startingPosition">Where in array a that you'd like to start the new sub arrary</param>
        /// <param name="length">Length of the new array</param>
        /// <returns>An array that is parsed.</returns>
        public static int[] SubArray(this int[] a, int startingPosition, int length)
        {
            int[] returnValue = new int[length];
            for (int i = 0; i < length; i++)
            {
                returnValue[i] = a[startingPosition + i];
            }
            return returnValue;
        }
    }
}
