﻿using System;
using NUnit.Framework;

namespace UnitTests.Respectable
{
    [TestFixture]
    public class MinMaxUnitTests
    {
        [Test]
        public void MinMaxDivisionTest01()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 2, 2 };
            Assert.AreEqual(6, Codility.Respectable.MinMaxDisivion.MinMaxDivision(3, 5, a));
        }

        [Test]
        public void MinMaxDivisionTest02()
        {
            int[] a = new[] { 1, 2, 3, 5, 4, 2, 4 };
            //[1, 2 , 3, 5, 4, 2, 4], [], [] with a large sum of 20.
            //[1, 2], [3, 5, 4, 2, 4], []  with a large sum of 18.
            //[1, 2, 3] [5, 4] [2, 4]  with a large sum of 9.
            //[1, 2], [3, 5], [4, 2, 4] with a large sum of 10.

            Assert.AreEqual(9, Codility.Respectable.MinMaxDisivion.MinMaxDivision(3, 5, a));
        }

        [Test]
        public void MinMaxDivisionTest03()
        {
            int[] a = new[] { 1, 2, 3, 5, 4, 2, 4 };
            //[1, 2, 3, 5, 4, 2, 4], [] with a large sum of 21.
            //[1, 2, 3], [5, 4, 2, 4] with a large sum of 15.
            // [1, 2, 3, 5], [4, 2, 4] with a large sum of 11.

            Assert.AreEqual(11, Codility.Respectable.MinMaxDisivion.MinMaxDivision(2, 5, a));
        }

        [Test]
        public void MinMaxDivisionTest05()
        {
            int[] a = new[] { 1, 2, 3, 4 };
            //[1, 2, 3, 4], []. 10
            //[1, 2, 3], [4]. 6
            //[1, 2], [3, 4]. 7
            //[1], [2, 3, 4]. 9

            Assert.AreEqual(6, Codility.Respectable.MinMaxDisivion.MinMaxDivision(2, 5, a));
        }


        [Test]
        public void MinMaxDivisionTest06()
        {
            int[] a = new[] { 1, 2 };
            //[1, 2, [] 3
            //[1], [2] 2
            Assert.AreEqual(2, Codility.Respectable.MinMaxDisivion.MinMaxDivision(2, 5, a));
        }

        [Test]
        public void MinMaxDivisionTest07()
        {
            int[] a = new[] { 1, 2, 3, 4 };
            //[1, 2, 3, 4], [], []. 
            //[1, 2, 3], [4], []. 6
            //[1, 2], [3, 4], []. 7
            //[1, 2], [3], [4]. 4
            //[1], [2, 3, 4], []. 9
            //[1], [2, 3], [4]. 5
            //[1], [2], [3, 4]. 7

            Assert.AreEqual(4, Codility.Respectable.MinMaxDisivion.MinMaxDivision(3, 5, a));
        }
        [Test]
        public void MinMaxDivisionTest08()
        {
            int[] a = new[] { 3, 4 };
            //[3, 4]

            Assert.AreEqual(7, Codility.Respectable.MinMaxDisivion.MinMaxDivision(1, 5, a));
        }

        [Test]
        public void MinMaxDivisionTest09()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 2, 7, 3 };

            // [2, 1, 5, 1, 2, 2, 7, 3] [] [] [] [] 24
            // [2, 1, 5, 1, 2, 2, 7], [3] [] [] [] 21
            // [2, 1, 5, 1, 2, 2], [7, 3] [] [] [] 14
            // [2, 1, 5, 1, 2] [2, 7, 3] [] [] [] 12
            // [2, 1, 5, 1], [2, 2, 7, 3] [] [] [] 14
            // [2, 1, 5, 1], [2, 2, 7], [3] [] [] 11
            // [2, 1, 5, 1], [2, 2], [7], [3] [] 10
            // [2, 1, 5], [1], [2, 2], [7], [3] 7


            Assert.AreEqual(7, Codility.Respectable.MinMaxDisivion.MinMaxDivision(5, 7, a));
        }
        [Test]
        public void MinMaxDivisionTest10()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 2, 7, 3, 15, 5, 22, 1000, 81, 2, 6 };
            Assert.AreEqual(1000, Codility.Respectable.MinMaxDisivion.MinMaxDivision(5, 7, a));
        }


        [Test]
        public void MinMaxDivisionTest11()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 0, 7, 3, 15, 5, 22, 1000, 81, 2, 6 };
            Assert.AreEqual(1063, Codility.Respectable.MinMaxDisivion.MinMaxDivision(2, 1000, a));
        }


        [Test]
        public void MinMaxDivisionTest12()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3 };
            Assert.AreEqual(27, Codility.Respectable.MinMaxDisivion.MinMaxDivision(4, 22, a));
        }

        [Test]
        public void MinMaxDivisionTest13Stress()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3, 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3, 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3, 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3 };
            Assert.AreEqual(89, Codility.Respectable.MinMaxDisivion.MinMaxDivision(4, 22, a));
        }

        [Test]
        public void MinMaxDivisionTest14Stress()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3, 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3, 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3, 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3 };
            Assert.AreEqual(178, Codility.Respectable.MinMaxDisivion.MinMaxDivision(2, 22, a));
        }
        [Test]
        public void MinMaxDivisionTest15Stress()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3 };
            Assert.AreEqual(22, Codility.Respectable.MinMaxDisivion.MinMaxDivision(7, 22, a));
        }

        [Test]
        public void MinMaxDivisionTest16Stress()
        {
            int[] a = new[] { 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3, 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3, 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3, 2, 1, 5, 1, 2, 0, 7, 3, 5, 5, 22, 6, 3, 2, 6, 10, 6, 3 };
            Assert.AreEqual(76, Codility.Respectable.MinMaxDisivion.MinMaxDivision(5, 22, a));
        }

    }
}
