﻿using System.Collections.Generic;
using System.Linq;

namespace Codility.Ambitious
{
    /*
A non-empty zero-indexed array A consisting of N integers is given.

A monotonic pair is a pair of integers (P, Q), such that 0 ≤ P ≤ Q < N and A[P] ≤ A[Q].

The goal is to find the monotonic pair whose indices are the furthest apart. More precisely, we should maximize the value Q − P. It is sufficient to find only the distance.

For example, consider array A such that:
    A[0] = 5
    A[1] = 3
    A[2] = 6
    A[3] = 3
    A[4] = 4
    A[5] = 2

There are eleven monotonic pairs: (0,0), (0, 2), (1, 1), (1, 2), (1, 3), (1, 4), (2, 2), (3, 3), (3, 4), (4, 4), (5, 5). The biggest distance is 3, in the pair (1, 4).

Write a function:

    class Solution { public int solution(int[] A); }

that, given a non-empty zero-indexed array A of N integers, returns the biggest distance within any of the monotonic pairs.

For example, given:
    A[0] = 5
    A[1] = 3
    A[2] = 6
    A[3] = 3
    A[4] = 4
    A[5] = 2

the function should return 3, as explained above.

Assume that:

        N is an integer within the range [1..300,000];
        each element of array A is an integer within the range [−1,000,000,000..1,000,000,000].

Complexity:

        expected worst-case time complexity is O(N);
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).

Elements of input arrays can be modified.

    */
    public class MaxDistanceMonotonic
    {
        public static int GetMaxDistanceMonotonic(int[] a)
        {
            //First get the monotonics out of the array
            List<int[]> monotonics = GetMonotonics(a);
            //Quick linq statement to sum up each difference then grab the biggest.
            return monotonics.Select(m => m[1] - m[0]).OrderByDescending(m => m).First();
        }

        private static List<int[]> GetMonotonics(int[] a)
        {
            List<int[]> monotonics = new List<int[]>();
            
            //Cycle through each int in the array and try to find monotonics
            for (int i = 0; i < a.Length; i ++)
            {
                int thisInt = a[i];
                //Loop through other ints farther into the array
                for (int j = i; j < a.Length; j ++)
                {
                    if (a[i] <= a[j])
                        monotonics.Add(new []{ i, j});
                } 
            }


            return monotonics;
        } 
    }
}
