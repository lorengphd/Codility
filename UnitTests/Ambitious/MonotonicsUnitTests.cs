﻿using System;
using NUnit.Framework;

namespace UnitTests.Ambitious
{
    [TestFixture]
    public class MonotonicsUnitTests
    {
        [Test]
        public void MonotonicsTest01()
        {
            int[] a = new[] { 5, 3, 6, 3, 4, 2 };
            // montonics: 
            // (0, 0), (0, 2)
            // (1, 1), (1, 2), (1, 3), (1, 4)
            //And so on

            Assert.AreEqual(3, Codility.Ambitious.MaxDistanceMonotonic.GetMaxDistanceMonotonic(a));
        }

        [Test]
        public void MonotonicsTest02()
        {
            int[] a = new[] { 1, 2, 5, 1 };
            // montonics: 
            // (0, 0), (0, 1), (0, 2), (0, 3)
            // (1, 1), (1, 2)
            // (2, 2)

            Assert.AreEqual(3, Codility.Ambitious.MaxDistanceMonotonic.GetMaxDistanceMonotonic(a));
        }

        [Test]
        public void MonotonicsTest03()
        {
            int[] a = new[] { 4, 5, 2, 9, 2 };
            // montonics: 
            // (0, 0), (0, 1), (0, 3)
            // (1, 1), (1, 3)
            // (2, 2), (2, 4)
            // (3, 3)

            Assert.AreEqual(3, Codility.Ambitious.MaxDistanceMonotonic.GetMaxDistanceMonotonic(a));
        }


        [Test]
        public void MonotonicsTest04()
        {
            int[] a = new[] { 12, 17, 2, 25, 85, 6 };
            // montonics: 
            // (0, 0), (0, 1), (0, 3), (0, 4)
            // (1, 1), (1, 3), (1, 4)
            // (2, 2), (2, 3), (2, 4), (2, 5)
            // (3, 3), (3, 4)

            Assert.AreEqual(4, Codility.Ambitious.MaxDistanceMonotonic.GetMaxDistanceMonotonic(a));
        }
        [Test]
        public void MonotonicsTest05()
        {
            int[] a = new[] { 85, 17, 2, 12, 27, 6 };
            // montonics: 
            // (0, 0)
            // (1, 1), (1, 4)
            // (2, 2), (2, 3), (2, 4), (2, 5)
            // (3, 3), (3, 4)

            Assert.AreEqual(3, Codility.Ambitious.MaxDistanceMonotonic.GetMaxDistanceMonotonic(a));
        }

        [Test]
        public void MonotonicsTest06()
        {
            int[] a = new[] { 5, 4, 3, 2, 1 };
            // montonics: 
            // (0, 0)
            // (1, 1)
            // (2, 2)
            // (3, 3)                     

            Assert.AreEqual(0, Codility.Ambitious.MaxDistanceMonotonic.GetMaxDistanceMonotonic(a));
        }

    }
}
