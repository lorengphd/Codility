﻿using System;
using System.Collections.Generic;
using Codility.Respectable;
using NUnit.Framework;
using static Codility.Respectable.SantasSleigh;


namespace UnitTests.Respectable
{
    [TestFixture]
    public class SantasSleighTests
    {
        private SantasSleigh _service = new SantasSleigh();

        [Test]
        public void SantasSleighTests01()
        {
            var coords = new List<Coord>(){
                new Coord(2, Direction.R),
                new Coord(2, Direction.R),
                new Coord(2, Direction.R),
            };

            Assert.AreEqual(2, _service.CalculateDistance(coords));
        }

        [Test]
        public void SantasSleighTests02()
        {
            var coords = new List<Coord>(){
                new Coord(2, Direction.R),
                new Coord(2, Direction.R),
                new Coord(2, Direction.L),
            };

            Assert.AreEqual(6, _service.CalculateDistance(coords));
        }

		[Test]
		public void SantasSleighTests03()
		{
			var coords = new List<Coord>(){
				new Coord(2, Direction.R),
				new Coord(2, Direction.R),
				new Coord(2, Direction.R),
				new Coord(2, Direction.R),
			};

			Assert.AreEqual(0, _service.CalculateDistance(coords));
		}


		[Test]
		public void SantasSleighTests04()
		{
			var coords = new List<Coord>(){
				new Coord(2, Direction.R),
				new Coord(2, Direction.L),
				new Coord(2, Direction.R),
				new Coord(2, Direction.L),
			};

			Assert.AreEqual(8, _service.CalculateDistance(coords));
		}

		[Test]
		public void SantasSleighTests05()
		{
            var coords = SantasSleigh.CreateCords("L3, R2, L5, R1, L1, L2, L2, R1, R5, R1, L1, L2, R2, R4, L4, L3, L3, R5, L1, R3, L5, L2, R4, L5, R4, R2, L2, L1, R1, L3, L3, R2, R1, L4, L1, L1, R4, R5, R1, L2, L1, R188, R4, L3, R54, L4, R4, R74, R2, L4, R185, R1, R3, R5, L2, L3, R1, L1, L3, R3, R2, L3, L4, R1, L3, L5, L2, R2, L1, R2, R1, L4, R5, R4, L5, L5, L4, R5, R4, L5, L3, R4, R1, L5, L4, L3, R5, L5, L2, L4, R4, R4, R2, L1, L3, L2, R5, R4, L5, R1, R2, R5, L2, R4, R5, L2, L3, R3, L4, R3, L2, R1, R4, L5, R1, L5, L3, R4, L2, L2, L5, L5, R5, R2, L5, R1, L3, L2, L2, R3, L3, L4, R2, R3, L1, R2, L5, L3, R4, L4, R4, R3, L3, R1, L3, R5, L5, R1, R5, R3, L1");

			Assert.AreEqual(209, _service.CalculateDistance(coords));
		}




		/* PART 2 */

		[Test]
		public void SantasSleighPart2Test01()
		{
			var coords = SantasSleigh.CreateCords("R8, R4, R4, R4, R4");

			Assert.AreEqual(4, _service.CalculateDistancePhase2(coords));
		}

		[Test]
		public void SantasSleighPart2Test02()
		{
			var coords = SantasSleigh.CreateCords("L3, R2, L5, R1, L1, L2, L2, R1, R5, R1, L1, L2, R2, R4, L4, L3, L3, R5, L1, R3, L5, L2, R4, L5, R4, R2, L2, L1, R1, L3, L3, R2, R1, L4, L1, L1, R4, R5, R1, L2, L1, R188, R4, L3, R54, L4, R4, R74, R2, L4, R185, R1, R3, R5, L2, L3, R1, L1, L3, R3, R2, L3, L4, R1, L3, L5, L2, R2, L1, R2, R1, L4, R5, R4, L5, L5, L4, R5, R4, L5, L3, R4, R1, L5, L4, L3, R5, L5, L2, L4, R4, R4, R2, L1, L3, L2, R5, R4, L5, R1, R2, R5, L2, R4, R5, L2, L3, R3, L4, R3, L2, R1, R4, L5, R1, L5, L3, R4, L2, L2, L5, L5, R5, R2, L5, R1, L3, L2, L2, R3, L3, L4, R2, R3, L1, R2, L5, L3, R4, L4, R4, R3, L3, R1, L3, R5, L5, R1, R5, R3, L1");

			Assert.AreEqual(136, _service.CalculateDistancePhase2(coords));
		}

	}
}
