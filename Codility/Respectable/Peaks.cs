﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Codility.Respectable
{
    /*


A non-empty zero-indexed array A consisting of N integers is given.

A peak is an array element which is larger than its neighbors. More precisely, it is an index P such that 0 < P < N − 1,  A[P − 1] < A[P] and A[P] > A[P + 1].

For example, the following array A:
    A[0] = 1
    A[1] = 2
    A[2] = 3
    A[3] = 4
    A[4] = 3
    A[5] = 4
    A[6] = 1
    A[7] = 2
    A[8] = 3
    A[9] = 4
    A[10] = 6
    A[11] = 2

has exactly three peaks: 3, 5, 10.

We want to divide this array into blocks containing the same number of elements. More precisely, we want to choose a number K that will yield the following blocks:

        A[0], A[1], ..., A[K − 1],
        A[K], A[K + 1], ..., A[2K − 1],
        ...
        A[N − K], A[N − K + 1], ..., A[N − 1].

What's more, every block should contain at least one peak. Notice that extreme elements of the blocks (for example A[K − 1] or A[K]) can also be peaks, but only if they have both neighbors (including one in an adjacent blocks).

The goal is to find the maximum number of blocks into which the array A can be divided.

Array A can be divided into blocks as follows:

        one block (1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2). This block contains three peaks.
        two blocks (1, 2, 3, 4, 3, 4) and (1, 2, 3, 4, 6, 2). Every block has a peak.
        three blocks (1, 2, 3, 4), (3, 4, 1, 2), (3, 4, 6, 2). Every block has a peak. Notice in particular that the first block (1, 2, 3, 4) has a peak at A[3], because A[2] < A[3] > A[4], even though A[4] is in the adjacent block.

However, array A cannot be divided into four blocks, (1, 2, 3), (4, 3, 4), (1, 2, 3) and (4, 6, 2), because the (1, 2, 3) blocks do not contain a peak. Notice in particular that the (4, 3, 4) block contains two peaks: A[3] and A[5].

The maximum number of blocks that array A can be divided into is three.

Write a function:

    int solution(int A[], int N);

that, given a non-empty zero-indexed array A consisting of N integers, returns the maximum number of blocks into which A can be divided.

If A cannot be divided into some number of blocks, the function should return 0.

For example, given:
    A[0] = 1
    A[1] = 2
    A[2] = 3
    A[3] = 4
    A[4] = 3
    A[5] = 4
    A[6] = 1
    A[7] = 2
    A[8] = 3
    A[9] = 4
    A[10] = 6
    A[11] = 2

the function should return 3, as explained above.

Assume that:

        N is an integer within the range [1..100,000];
        each element of array A is an integer within the range [0..1,000,000,000].

Complexity:

        expected worst-case time complexity is O(N*log(log(N)));
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).

Elements of input arrays can be modified.


    */
    public class Peaks
    {
        public static int PeaksSolution(int[] a, int n)
        {
            int[] peaks = getPeaks(a);

            //Iterate through possibilities starting at best and moving backwards.
            for (int o = peaks.Length; o > 0; o--)
            {
                if (a.Length % o != 0)
                {
                    //Not evenly divisible, move on...
                    continue;
                }

                //Split the main array into sub arrays and test to see if there's a peak in each.
                List<int[]> splits = SplitArrayEvenly(a, o);
                bool itsGood = false;
                for (int i = 0; i < splits.Count; i++)
                {
                    //This variable refers to the starting and ending spot that this split exists in the main array
                    int[] thisSplitsPosition = new int[2];
                    thisSplitsPosition[0] = i * splits[i].Length;
                    thisSplitsPosition[1] = thisSplitsPosition[0] + splits[i].Length - 1;

                    //Validate that a peak exists in this split.
                    if (peaks.Any(p => p >= thisSplitsPosition[0] && p <= thisSplitsPosition[1]))
                    {
                        itsGood = true;
                    }
                    else
                    {
                        itsGood = false;
                    }
                }
                if (itsGood) return o;
            }
            return 0;
        }

        /// <summary>
        /// Evenly divide an array into smaller arrays.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="splitCount">The amounts of splits that you'd like returned.</param>
        /// <returns>A list of smaller arrays.</returns>
        private static List<int[]> SplitArrayEvenly(int[] a, int splitCount)
        {
            if (a.Length % splitCount != 0)
                throw new ArgumentException("Cannot evenly split array with this count.");

            int intsPerSplit = a.Length / splitCount;
            List<int[]> splits = new List<int[]>();

            for (int i = 0; i < splitCount; i++)
            {
                int startingPosition = intsPerSplit * i;
                int[] split = a.SubArray(startingPosition, intsPerSplit);
                splits.Add(split);
            }
            return splits;
        }

        /// <summary>
        /// Detect and return the peaks in an array.
        /// </summary>
        /// <returns>An array describing the positions of all peaks</returns>
        private static int[] getPeaks(int[] a)
        {
            List<int> peaks = new List<int>();
            for (int i = 0; i < a.Length; i++)
            {
                if (i == 0 || i == a.Length - 1)
                    continue;

                if (a[i - 1] < a[i] && a[i] > a[i + 1])
                {
                    peaks.Add(i);
                }
            }
            return peaks.ToArray();
        }
    }
}